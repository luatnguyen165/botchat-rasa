# This files contains your custom actions which can be used to run
# custom Python code.
#
# See this guide on how to implement these action:
# https://rasa.com/docs/rasa/custom-actions


# This is a simple example for a custom action which utters "Hello World!"

from typing import Any, Text, Dict, List

from rasa_sdk import Action, Tracker
from rasa_sdk.executor import CollectingDispatcher


class ActionHelloWorld(Action):

    def name(self) -> Text:
        return "action_resources_list"

    def run(self, dispatcher: CollectingDispatcher,
            tracker: Tracker,
            domain: Dict[Text, Any]) -> List[Dict[Text, Any]]:
    
        # buttons = [{"title": "Rohan Shah", "payload": "/intent_name"}, {"title": "Rohan Pandya", "payload": "/intent_name"}]
        # dispatcher.utter_button_message("There are 2 people with the name Rohan:", buttons)
        message = {
            "type": "template",
            "payload": {
                "template_type": "generic",
                "elements": [
                    {
                        "title": "Trà sữa chân châu đường đen",
                        "subtitle": "$10",
                        "image_url": "https://feelingteaonline.com/wp-content/uploads/2020/08/s%C6%B0a-tuoi-tc-%C4%91%C6%B0%E1%BB%9Dng-%C4%91en.jpg",
                        "buttons": [ 
                            {
                            "title": "Đặt hàng",
                            "payload": "/order",
                            "type": "postback"
                            },
                            {
                            "title": "Đánh giá",
                            "payload": "sad",
                            "type": "postback"
                            }
                        ]
                    },
                    {
                        "title": "Trà sữa thái xanh",
                        "subtitle": "$12",
                        "image_url": "https://trasuasam.com/uploads/source/tra-sua/tra-sua-thai-xanh.jpg",
                        "buttons": [ 
                            {
                            "title": "Đặt hàng",
                            "payload": "/order",
                            "type": "postback"
                            },
                            {
                            "title": "Đánh giá",
                            "payload": "sad",
                            "type": "postback"
                            }
                        ]
                    },
                    {
                        "title": "Trà sữa socola",
                        "subtitle": "$12",
                        "image_url": "https://jarvis.vn/wp-content/uploads/2019/05/tra%CC%80-su%CC%83a-socola.jpg",
                        "buttons": [ 
                            {
                            "title": "Đặt hàng",
                            "payload": "/order",
                            "type": "postback"
                            },
                            {
                            "title": "Đánh giá",
                            "payload": "sad",
                            "type": "postback"
                            }
                        ]
                    }
                ]
                }
        }
        dispatcher.utter_message(attachment=message)
        return []
